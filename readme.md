# super-status

![Screenshot](screenshot.png "title")

Featureful system status info fetcher

**Supports:**

- [swaybar-protocol](https://man.archlinux.org/man/swaybar-protocol.7)

## Install

### On NixOS

	nix-env -i -f default.nix

### On other distros

	zig build install --release=fast --prefix ~/.local

```sh
# ~/.profile
export PATH="$HOME/.local/bin:$PATH"
```

## Features

**Blocks:**

- Cpu load
- Ram usage
- Disk usage
- Battery status
- Pipewire sink volume
- Date and time

