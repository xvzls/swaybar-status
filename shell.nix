{
	pkgs ? import <nixpkgs> { },
	unstable ? import (fetchTarball "https://channels.nixos.org/nixos-unstable/nixexprs.tar.xz") {},
	zon2nix ? pkgs.callPackage (builtins.fetchGit {
		url = "https://github.com/xvzls/zon2nix";
		rev = "9d6b1c0ebaf792a23987cc75c4a5677acdca1283";
	}) {},
}:
pkgs.mkShell {
	nativeBuildInputs = [
		unstable.zig
		unstable.zls
		pkgs.pkg-config
		zon2nix
	];
	
	buildInputs = [
		pkgs.pipewire
	];
}
