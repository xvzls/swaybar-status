const std = @import("std");

pub fn build(b: *std.Build) void {
	const target = b.standardTargetOptions(.{});
	const optimize = b.standardOptimizeOption(.{});
	
	// Executable
	
	const exe = b.addExecutable(.{
		.name = "super-status",
		.root_source_file = b.path("src/main.zig"),
		.target = target,
		.optimize = optimize,
		.single_threaded = true,
		.strip = (
			optimize == .ReleaseFast or
			optimize == .ReleaseSmall
		),
	});
	
	const dependencies = [_][]const u8{
		"memory-info",
		"power-info",
		"cpu-info",
		"fs-info",
		"chronic",
		"json-fmt",
		"byte-units",
		"pw-sinks",
		"regex",
	};
	
	for (dependencies) |name| {
		const module = b.dependency(name, .{
			.target = target,
			.optimize = optimize,
		}).module(name);
		
		exe.root_module.addImport(name, module);
	}
	
	b.installArtifact(exe);
	
	const run_cmd = b.addRunArtifact(exe);
	run_cmd.step.dependOn(b.getInstallStep());
	
	if (b.args) |args| {
		run_cmd.addArgs(args);
	}
	
	b.step(
		"run",
		"Run executable"
	).dependOn(&run_cmd.step);
	
	const tests = b.addTest(.{
		.root_source_file = b.path("src/main.zig"),
		.target = target,
		.optimize = .ReleaseFast,
	});
	
	for (dependencies) |name| {
		const module = b.dependency(name, .{
			.target = target,
			.optimize = optimize,
		}).module(name);
		
		tests.root_module.addImport(name, module);
	}
	
	const run_tests = b.addRunArtifact(tests);
	
	const test_step = b.step(
		"test",
		"Run unit tests"
	);
	test_step.dependOn(&run_tests.step);
}

