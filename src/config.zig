const root = @import("root.zig");
const std = @import("std");

const power_info = @import("power-info");

const BlockBuilder = root.block.BlockBuilder;
const Format = root.block.Format;

pub const Config = struct {
	main: struct {
		delay: u32 = 1_000,
		@"separator-width": u8 = 2,
	} = .{},
	colors: root.block.ColorMap = .{},
	block: []root.block.Config,
	
	pub const Json = std.json.Parsed(@This());
	
	pub inline fn parse(
		allocator: std.mem.Allocator,
		string: []const u8
	) !Json {
		return std.json.parseFromSlice(
			@This(),
			allocator,
			string,
			.{}
		);
	}
	
	pub fn default(
		allocator: std.mem.Allocator
	) !Json {
		return @This().parse(allocator,
			\\{"block": [
			\\  {"cpu": {}},
			\\  {"memory": {}},
			\\  {"file_system": {}},
			\\  {"mixer": {}},
			\\  {"power": {}},
			\\  {"datetime": {}}
			\\]}
		);
	}
	
	pub fn defaultJson(
		allocator: std.mem.Allocator,
	) ![]const u8 {
		var d = try @This().default(allocator);
		defer d.deinit();
		return std.json.stringifyAlloc(
			allocator,
			d.value,
			.{
				.whitespace = .indent_2,
			}
		);
	}
	
	pub fn get(
		allocator: std.mem.Allocator
	) !Json {
		const openDir = std.fs.openDirAbsolute;
		
		const path = std.process.getEnvVarOwned(
			allocator,
			"HOME"
		) catch return @This().default(allocator);
		defer allocator.free(path);
		
		var dir = try openDir(path, .{});
		defer dir.close();
		
		var file = dir.openFile(
			".config/super-status/config.json",
			.{}
		) catch return @This().default(allocator);
		defer file.close();
		
		const string = try file.readToEndAlloc(
			allocator,
			1024
		);
		defer allocator.free(string);
		
		return @This().parse(allocator, string);
	}
};

