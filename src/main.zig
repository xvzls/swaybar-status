const std = @import("std");
const bar = @import("bar.zig");
const root = @import("root.zig");

fn help(
	program: []const u8,
	writer: anytype,
) !void {
	try writer.print(
		\\Usage: {s} [COMMAND]
		\\
		\\Commands:
		\\
		\\  example  Show config example
		\\  help     Show help
		\\
	, .{ program });
}

fn example(
	allocator: std.mem.Allocator,
	writer: anytype,
) !void {
	const json = try root.config.Config.defaultJson(
		allocator
	);
	defer allocator.free(json);
	
	try writer.print("{s}\n", .{ json });
}

pub fn main() !void {
	const allocator = std.heap.page_allocator;
	const out = std.io.getStdOut().writer();
	
	var args = std.process.args();
	const program = args.next().?;
	
	const command = args.next() orelse {
		var buffered = std.io.bufferedWriter(out);
		try bar.run(allocator, buffered.writer());
		return;
	};
	
	if (std.mem.eql(u8, command, "help")) {
		try help(program, out);
		return;
	}
	
	if (std.mem.eql(u8, command, "example")) {
		try example(allocator, out);
		return;
	}
	
	const err = std.io.getStdErr().writer();
	try help(program, err);
	return error.InvalidCommand;
}

test {
	std.testing.refAllDecls(@This());
}

