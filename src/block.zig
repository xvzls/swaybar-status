const root = @import("root.zig");
const std = @import("std");

pub const datetime = @import("block/datetime.zig");
pub const power = @import("block/power.zig");
pub const mixer = @import("block/mixer.zig");
pub const cpu = @import("block/cpu.zig");
pub const memory = @import("block/memory.zig");
pub const fs = @import("block/fs.zig");

const Color = root.color.Color;

pub const Icon = union(enum) {
	None,
	Circle,
	Hexagon,
	Volume,
	Clock,
	Power: enum {
		Charging,
		Discharging,
		Unknown,
		Full,
	},
	
	inline fn singular(
		icons: []const []const u8,
		round_up: bool,
		percent: f32,
	) ![]const u8 {
		if (icons.len == 0) {
			return error.NoIconVariants;
		}
		
		const len: f32 = @floatFromInt(icons.len);
		const index: u8 = @intFromFloat(percent * len);
		
		if (index >= icons.len) {
			return icons[icons.len - 1];
		}
		
		// Makes sure stuff like 1% volume aren't shown
		// as muted.
		if (
			!round_up and
			percent > 0.0 and
			index == 0 and
			icons.len > 1
		) {
			return icons[1];
		}
		
		return icons[index];
	}
	
	pub fn toString(
		this: @This(),
		percent: f32,
	) !?[]const u8 {
		const info: struct {
			icons: []const []const u8,
			round_up: bool = true,
		} = switch (this) {
			.None => return null,
			.Circle => .{
				.icons = &.{ "󰄰", "󰪞", "󰪟", "󰪠", "󰪡", "󰪢", "󰪣", "󰪤", "󰪥", },
			},
			.Hexagon => .{
				.icons = &.{ "󰋙", "󰫃", "󰫄", "󰫅", "󰫆", "󰫇", "󰫈", },
			},
			.Volume => .{
				.icons = &.{ "", "", "", },
				.round_up = false,
			},
			.Clock => .{
				.icons = &.{ "󱑖", "󱑋", "󱑌", "󱑍", "󱑎", "󱑏", "󱑐", "󱑑", "󱑒", "󱑓", "󱑔", "󱑕", },
			},
			.Power => |status| .{
				.icons = switch (status) {
					.Charging => &.{ "󰢟", "󰢜", "󰂆", "󰂇", "󰂈", "󰢝", "󰂉", "󰢞", "󰂊", "󰂋", "󰂅", },
					.Discharging => &.{ "󰂎", "󰁺", "󰁻", "󰁼", "󰁽", "󰁾", "󰁿", "󰂀", "󰂁", "󰂂", "󰁹", },
					.Unknown => &.{ "󱟨", },
					.Full => &.{ "󱈏", },
				},
			},
		};
		
		return try singular(
			info.icons,
			info.round_up,
			percent,
		);
	}
};

pub const BadWhen = union(enum) {
	over: struct {
		warn: f32,
		critical: f32,
	},
	under: struct {
		warn: f32,
		critical: f32,
	},
	none,
};

pub const ColorMap = struct {
	default: Color = .{ .inner = "#ffffffff".* },
	yellow: Color = .{ .inner = "#ffff77ff".* },
	red: Color = .{ .inner = "#ff7777ff".* },
	background: ?Color = null,
	
	pub fn resolve(
		this: *const @This(),
		bad_when: BadWhen,
		percent: f32,
	) Color {
		return switch (bad_when) {
			.over => |values|
				if (percent > values.critical)
					this.red
				else if (percent > values.warn)
					this.yellow
				else
					this.default,
			.under => |values|
				if (percent < values.critical)
					this.red
				else if (percent < values.warn)
					this.yellow
				else
					this.default,
			.none => this.default,
		};
	}
};

pub const Block = struct {
	allocator: std.mem.Allocator,
	string: ?[]u8,
	percent: f32,
	icon: ?[]const u8 = null,
	
	pub inline fn deinit(this: *@This()) void {
		if (this.string) |string| {
			this.allocator.free(string);
		}
	}
	
	pub fn format(
		this: @This(),
		comptime _: []const u8,
		_: std.fmt.FormatOptions,
		writer: anytype
	) !void {
		if (this.string) |string| {
			try writer.print("{s}", .{ string });
		}
		if (this.icon) |icon| {
			if (this.string != null) {
				try writer.writeByte(' ');
			}
			try writer.print("{s} ", .{ icon });
		}
	}
};

pub const Config = union(enum) {
	cpu: cpu.Block.Config,
	memory: memory.Block.Config,
	file_system: fs.Block.Config,
	mixer: mixer.Block.Config,
	power: power.Block.Config,
	datetime: datetime.Block.Config,
	
	pub fn block(
		this: @This(),
		allocator: std.mem.Allocator,
	) !BlockBuilder {
		return switch (this) {
			.cpu => |config| .{
				.cpu = try cpu.Block.init(allocator, config)
			},
			.memory => |config| .{
				.memory = try memory.Block.init(allocator, config)
			},
			.file_system => |config| .{
				.file_system = try fs.Block.init(allocator, config)
			},
			.mixer => |config| .{
				.mixer = try mixer.Block.init(allocator, config)
			},
			.power => |config| .{
				.power = try power.Block.init(allocator, config)
			},
			.datetime => |config| .{
				.datetime = try datetime.Block.init(allocator, config)
			},
		};
	}
};

pub const BlockBuilder = union(enum) {
	cpu: cpu.Block,
	memory: memory.Block,
	file_system: fs.Block,
	mixer: mixer.Block,
	power: power.Block,
	datetime: datetime.Block,
	
	pub inline fn now(this: *@This()) !?Block {
		return switch (this.*) {
			.cpu         => |*ref| ref.now(),
			.memory      => |*ref| ref.now(),
			.file_system => |*ref| ref.now(),
			.mixer       => |*ref| ref.now(),
			.power       => |*ref| ref.now(),
			.datetime    => |*ref| ref.now(),
		};
	}
	
	pub inline fn getBadWhen(this: *const @This()) BadWhen {
		return switch (this.*) {
			.cpu         => |ref| ref.config.bad_when,
			.memory      => |ref| ref.config.bad_when,
			.file_system => |ref| ref.config.bad_when,
			.mixer       => |ref| ref.config.bad_when,
			.power       => |ref| ref.config.bad_when,
			.datetime    => |ref| ref.config.bad_when,
		};
	}
};

