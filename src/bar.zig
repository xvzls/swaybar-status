const json_fmt = @import("json-fmt");
const root = @import("root.zig");
const std = @import("std");

const Config = root.config.Config;
const Json = root.block.Json;

pub fn run(
	allocator: std.mem.Allocator,
	out: anytype,
) !void {
	var config_json = try Config.get(allocator);
	defer config_json.deinit();
	const config = config_json.value;
	
	try std.json.stringify(
		.{ .version = 1, },
		.{ .whitespace = .minified },
		out,
	);
	try out.writeByte('\n');
	
	var writer = std.json.writeStream(out, .{
		.emit_null_optional_fields = false,
		.whitespace = .indent_2,
	});
	try writer.beginArray();
	
	const blocks = try allocator.alloc(
		root.block.BlockBuilder,
		config.block.len
	);
	defer allocator.free(blocks);
	
	for (config.block, 0 ..) |block_config, i| {
		blocks[i] = try block_config.block(allocator);
		// TODO: free blocks
	}
	
	while (true) {
		const start = std.time.milliTimestamp();
		try writer.beginArray();
		
		for (blocks) |*block_todo| {
			var block = try
				block_todo.now()
			orelse
				continue;
			defer block.deinit();
			
			try writer.write(.{
				.full_text = json_fmt.format(
					"{}",
					.{ block },
				),
				.color = config.colors.resolve(
					block_todo.getBadWhen(),
					block.percent,
				),
				.background = config.colors.background,
				.separator_block_width = config.main.@"separator-width",
			});
		}
		
		try writer.endArray();
		try writer.stream.context.flush();
		
		const end = std.time.milliTimestamp();
		const took = end - start;
		const diff = @as(i64, config.main.delay) - took;
		if (std.math.cast(u64, diff)) |delay| {
			std.time.sleep(delay * std.time.ns_per_ms);
		}
	}
}

