const std = @import("std");

pub const Color = struct {
	inner: [9]u8,
	
	pub fn jsonParse(
		_: std.mem.Allocator,
		source: anytype,
		_: std.json.ParseOptions,
	) !@This() {
		const string = switch (try source.next()) {
			.string => |inner| inner,
			else => return error.UnexpectedToken,
		};
		
		if (string.len != 7 and string.len != 9) {
			return error.UnexpectedToken;
		}
		
		if (string[0] != '#') {
			return error.UnexpectedToken;
		}
		
		if (std.mem.indexOfNonePos(
			u8,
			string,
			1,
			"0123456789abcdefABCDEF"
		)) |_| {
			return error.UnexpectedToken;
		}
		
		var inner: [9]u8 = "#______ff".*;
		std.mem.copyForwards(u8, &inner, string);
		
		return .{
			.inner = inner,
		};
	}
	
	pub fn jsonStringify(
		this: *const @This(),
		writer: anytype
	) !void {
		try writer.print("\"{s}\"", .{ this.inner });
	}
};

test "color" {
	const allocator = std.testing.allocator;
	
	for ([_]struct { []const u8, []const u8 }{
		.{ "\"#000000\"",   "#000000ff"},
		.{ "\"#c0ffee\"",   "#c0ffeeff"},
		.{ "\"#fffFFfFF\"", "#fffFFfFF"},
	}) |tuple| {
		var color = try std.json.parseFromSlice(
			Color,
			allocator,
			tuple[0],
			.{}
		);
		defer color.deinit();
		
		try std.testing.expectEqualStrings(
			tuple[1],
			&color.value.inner,
		);
	}
}

