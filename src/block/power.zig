const power_info = @import("power-info");
const root = @import("../root.zig");
const std = @import("std");

pub const Block = struct {
	allocator: std.mem.Allocator,
	config: Config,
	
	pub const Config = struct {
		bad_when: root.block.BadWhen = .{
			.under = .{
				.warn = 0.30,
				.critical = 0.15,
			},
		},
		format: enum {
			None,
			Tiny,
			Full,
		} = .Tiny,
		icon: enum {
			None,
			Circle,
			Hexagon,
			Power,
		} = .Power,
	};
	
	pub fn init(
		allocator: std.mem.Allocator,
		config: Config,
	) !@This() {
		return .{
			.allocator = allocator,
			.config = config,
		};
	}
	
	pub fn now(this: *@This()) !?root.block.Block {
		var iter = try power_info.Fetcher.iterate();
		defer iter.close();
		
		var fetcher = try
			iter.next()
		orelse
			return null;
		defer fetcher.close();
		
		const capacity = try
			fetcher.getCapacity()
		orelse
			return null;
		const status = try
			fetcher.getStatus()
		orelse
			return null;
		
		const string = switch (this.config.format) {
			.None => null,
			.Tiny => try std.fmt.allocPrint(
				this.allocator,
				"{}%",
				.{ capacity },
			),
			.Full => try std.fmt.allocPrint(
				this.allocator,
				"{s} {}%",
				.{
					@tagName(status),
					capacity
				},
			),
		};
		
		const percent = @as(
			f32,
			@floatFromInt(capacity)
		) / 100.0;
		
		const icon: root.block.Icon = switch (
			this.config.icon
		) {
			.None => .None,
			.Circle => .Circle,
			.Hexagon => .Hexagon,
			.Power => .{
				.Power = switch (status) {
					.@"Charging" => .Charging,
					.@"Not charging",
					.@"Discharging" => .Discharging,
					.@"Unknown" => .Unknown,
					.@"Full" => .Full,
				},
			},
		};
		
		return .{
			.allocator = this.allocator,
			.string = string,
			.percent = percent,
			.icon = try icon.toString(percent),
		};
	}
};

