const fs_info = @import("fs-info");
const root = @import("../root.zig");
const std = @import("std");

const Size = @import("byte-units").Size;

pub const Block = struct {
	allocator: std.mem.Allocator,
	config: Config,
	
	pub const Config = struct {
		bad_when: root.block.BadWhen = .{
			.over = .{
				.warn = 0.80,
				.critical = 0.90,
			},
		},
		format: enum {
			None,
			Tiny,
			Full,
		} = .Tiny,
		icon: enum {
			None,
			Circle,
			Hexagon,
		} = .Hexagon,
		path: [:0]u8 = @constCast("/"),
	};
	
	pub fn init(
		allocator: std.mem.Allocator,
		config: Config,
	) !@This() {
		return .{
			.allocator = allocator,
			.config = config,
		};
	}
	
	pub fn now(this: *@This()) !?root.block.Block {
		const stat = try fs_info.Stat.at(
			this.config.path
		);
		
		const blocks = stat.blocks;
		const percent = blocks.getPercent();
		const string = switch (this.config.format) {
			.None => null,
			.Tiny => try std.fmt.allocPrint(
				this.allocator,
				"{:.1}",
				.{
					Size.new(blocks.getUsed()),
				},
			),
			.Full => try std.fmt.allocPrint(
				this.allocator,
				"Disk {:.1}/{:.1}",
				.{
					Size.new(blocks.getUsed()),
					Size.new(blocks.getTotal()),
				},
			),
		};
		
		const icon: root.block.Icon = switch (
			this.config.icon
		) {
			.None => .None,
			.Circle => .Circle,
			.Hexagon => .Hexagon,
		};
		
		return .{
			.allocator = this.allocator,
			.string = string,
			.percent = percent,
			.icon = try icon.toString(percent),
		};
	}
};

