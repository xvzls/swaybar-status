const cpu_info = @import("cpu-info");
const root = @import("../root.zig");
const std = @import("std");

threadlocal var READER = cpu_info.Time.reader();

pub const Block = struct {
	allocator: std.mem.Allocator,
	config: Config,
	
	pub const Config = struct {
		bad_when: root.block.BadWhen = .{
			.over = .{
				.warn = 0.80,
				.critical = 0.90,
			},
		},
		format: enum {
			None,
			Tiny,
			Full,
		} = .Tiny,
		icon: enum {
			None,
			Circle,
			Hexagon,
		} = .Circle,
	};
	
	pub fn init(
		allocator: std.mem.Allocator,
		config: Config,
	) !@This() {
		return .{
			.allocator = allocator,
			.config = config,
		};
	}
	
	pub fn now(this: *@This()) !?root.block.Block {
		const percent = try READER.usage();
		
		const string = switch (this.config.format) {
			.None => null,
			.Tiny => try std.fmt.allocPrint(
				this.allocator,
				"{d:.0}%",
				.{ percent * 100.0 },
			),
			.Full => try std.fmt.allocPrint(
				this.allocator,
				"Cpu {d:.0}%",
				.{ percent * 100.0 },
			),
		};
		
		const icon: root.block.Icon = switch (
			this.config.icon
		) {
			.None => .None,
			.Circle => .Circle,
			.Hexagon => .Hexagon,
		};
		
		return .{
			.allocator = this.allocator,
			.string = string,
			.percent = percent,
			.icon = try icon.toString(percent),
		};
	}
};

