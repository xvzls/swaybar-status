const memory_info = @import("memory-info");
const root = @import("../root.zig");
const std = @import("std");

const Size = @import("byte-units").Size;

pub const Block = struct {
	allocator: std.mem.Allocator,
	config: Config,
	
	pub const Config = struct {
		bad_when: root.block.BadWhen = .{
			.over = .{
				.warn = 0.80,
				.critical = 0.90,
			},
		},
		format: enum {
			None,
			Tiny,
			Full,
		} = .Tiny,
		icon: enum {
			None,
			Circle,
			Hexagon,
		} = .Hexagon,
		kind: enum {
			Ram,
			Swap,
		} = .Ram,
	};
	
	pub fn init(
		allocator: std.mem.Allocator,
		config: Config,
	) !@This() {
		return .{
			.allocator = allocator,
			.config = config,
		};
	}
	
	pub fn now(this: *@This()) !?root.block.Block {
		const memory = try memory_info.Memory.now();

		const name,
		const percent,
		const used,
		const total = switch (this.config.kind) {
			.Ram => ram: {
				const ram = memory.getRam();
				break :ram .{
					"Ram",
					ram.getPercent(),
					ram.getUsed(),
					ram.getTotal(),
				};
			},
			.Swap => swap: {
				const swap = memory.getSwap();
				break :swap .{
					"Swap",
					swap.getPercent(),
					swap.getUsed(),
					swap.getTotal(),
				};
			},
		};
		
		const string = switch (this.config.format) {
			.None => null,
			.Tiny => try std.fmt.allocPrint(
				this.allocator,
				"{:.1}",
				.{ Size.new(used) },
			),
			.Full => try std.fmt.allocPrint(
				this.allocator,
				"{s} {:.1}/{:.1}",
				.{
					name,
					Size.new(used),
					Size.new(total),
				},
			),
		};
		
		const icon: root.block.Icon = switch (
			this.config.icon
		) {
			.None => .None,
			.Circle => .Circle,
			.Hexagon => .Hexagon,
		};
		
		return .{
			.allocator = this.allocator,
			.string = string,
			.percent = percent,
			.icon = try icon.toString(percent),
		};
	}
};

