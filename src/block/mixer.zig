const pw_sinks = @import("pw-sinks");
const root = @import("../root.zig");
const std = @import("std");
const regex = @import("regex");

threadlocal var PW_INITIALIZED = false;

pub const Block = struct {
	allocator: std.mem.Allocator,
	config: Config,
	regex: regex.Regex,
	
	pub const Config = struct {
		bad_when: root.block.BadWhen = .{
			.over = .{
				.warn = 1.50,
				.critical = 1.60,
			},
		},
		format: enum {
			None,
			Tiny,
			Full,
		} = .Tiny,
		icon: enum {
			None,
			Circle,
			Hexagon,
			Volume,
		} = .Volume,
		regex: []u8 = @constCast(".*"),
	};
	
	pub fn init(
		allocator: std.mem.Allocator,
		config: Config,
	) !@This() {
		return .{
			.allocator = allocator,
			.config = config,
			.regex = try regex.Regex.compile(
				allocator,
				config.regex,
			),
		};
	}
	
	pub fn now(this: *@This()) !?root.block.Block {
		if (!PW_INITIALIZED) {
			pw_sinks.c.pw_init(null, null);
			PW_INITIALIZED = true;
		}
		
		var sink_reader = try pw_sinks.SinkReader.init(
			this.allocator
		);
		defer sink_reader.deinit();
		
		var sinks = try sink_reader.read();
		defer {
			for (sinks.items) |*sink| {
				sink.deinit();
			}
			sinks.deinit();
		}
		
		const sink = sink: {
			for (sinks.items) |sink| {
				if (try this.regex.match(sink.name)) {
					break :sink sink;
				}
			}
			return null;
		};
		
		const percent = percent: {
			var sum: f32 = 0.0;
			for (sink.volumes.items) |volume| {
				sum += volume;
			}
			break :percent sum / @as(f32,
				@floatFromInt(sink.volumes.items.len)
			);
		};
		
		const string = switch (this.config.format) {
			.None => null,
			.Tiny => try std.fmt.allocPrint(
				this.allocator,
				"{d:.0}%",
				.{ percent * 100.0 },
			),
			.Full => try std.fmt.allocPrint(
				this.allocator,
				"{s} {d:.0}%",
				.{
					sink.name,
					percent * 100.0,
				},
			),
		};
		
		const icon: root.block.Icon = switch (
			this.config.icon
		) {
			.None => .None,
			.Circle => .Circle,
			.Hexagon => .Hexagon,
			.Volume => .Volume,
		};
		
		return .{
			.allocator = this.allocator,
			.string = string,
			.percent = percent,
			.icon = try icon.toString(percent),
		};
	}
	
	// pub fn now(
	// 	this: *const @This(),
	// 	allocator: std.mem.Allocator,
	// ) !?root.block.Block {
	// 	var mixer = try
	// 		Mixer.now(allocator)
	// 	orelse
	// 		return null;
	// 	defer mixer.deinit();
		
	// 	var percent: f32 = 0.0;
	// 	for (mixer.volumes) |volume| {
	// 		percent += volume;
	// 	}
	// 	percent /= @floatFromInt(mixer.volumes.len);
		
	// 	const string = switch (this.format) {
	// 		.None => null,
	// 		.Tiny => try std.fmt.allocPrint(
	// 			allocator,
	// 			"{d:.0}%",
	// 			.{
	// 				percent * 100.0,
	// 			},
	// 		),
	// 		.Full => try std.fmt.allocPrint(
	// 			allocator,
	// 			"{s} {d:.0}%",
	// 			.{
	// 				mixer.name,
	// 				percent * 100.0,
	// 			},
	// 		),
	// 	};
		
	// 	return .{
	// 		.allocator = allocator,
	// 		.string = string,
	// 		.percent = percent,
	// 	};
	// }
};

