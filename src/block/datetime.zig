const chronic = @import("chronic");
const root = @import("../root.zig");
const std = @import("std");

const DateTime = chronic.DateTime;

fn toF32(value: anytype) f32 {
	return @floatFromInt(value);
}

pub const Block = struct {
	allocator: std.mem.Allocator,
	config: Config,
	
	pub const Config = struct {
		bad_when: root.block.BadWhen = .none,
		format: enum {
			None,
			Time,
			Tiny,
			Iso,
			IsoTz,
			Full,
		} = .Tiny,
		icon: enum {
			None,
			Circle,
			Hexagon,
			Clock,
		} = .Clock,
	};
	
	pub fn init(
		allocator: std.mem.Allocator,
		config: Config,
	) !@This() {
		return .{
			.allocator = allocator,
			.config = config,
		};
	}
	
	pub fn now(this: *@This()) !?root.block.Block {
		var datetime = try DateTime.local(
			this.allocator
		);
		defer datetime.deinit();
		
		const offsetted = datetime.getOffsetted();
		const percent = offsetted.time.getPercent();
		
		const string = switch (this.config.format) {
			.None => null,
			.Time => try std.fmt.allocPrint(
				this.allocator,
				"{:.0}",
				.{ offsetted.time },
			),
			.Tiny => try std.fmt.allocPrint(
				this.allocator,
				"{s} {:0>2} {:0>2}:{:0>2}",
				.{
					@tagName(offsetted.date.month),
					offsetted.date.day,
					offsetted.time.hour,
					offsetted.time.minute,
				},
			),
			.Iso => try std.fmt.allocPrint(
				this.allocator,
				"{:.0}",
				.{ offsetted },
			),
			.IsoTz, .Full => try std.fmt.allocPrint(
				this.allocator,
				"{:.0} {d:0>5.4}",
				.{
					offsetted,
					if (datetime.zone) |zone|
						@divTrunc(zone.getOffsetAt(
							offsetted.getTimestamp()
						), 3_600_000_000)
					else
						0
				},
			),
		};
		
		const half_day = offsetted.time.hour.inner % 12;
		const half_day_percent = toF32(half_day) / 12.0;
		const icon: root.block.Icon = switch (
			this.config.icon
		) {
			.None => .None,
			.Circle => .Circle,
			.Hexagon => .Hexagon,
			.Clock => .Clock,
		};
		
		return .{
			.allocator = this.allocator,
			.string = string,
			.percent = percent,
			.icon = try icon.toString(half_day_percent),
		};
	}
};

